import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

public class random {
    private ArrayList<String> list=new ArrayList<String>();
    public void generate(){

        Random random=new Random();
        String lookup="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        for(int x=0;x<30;x++){
            String rstr="";
            for(int y=0;y<random.nextInt(21)+10;y++)
                rstr+= lookup.charAt(random.nextInt(lookup.length())) ;
            list.add(rstr);
        }
    }

    public void write(String name) throws IOException {
        Files.write(Paths.get(name),list);
    }

    public void read_sort(String name) throws IOException{
        String sss;
        BufferedReader br=new BufferedReader(new FileReader(name));
        ArrayList<String> readed=new ArrayList<String>();
        while((sss=br.readLine())!=null)
            readed.add(sss);

        readed.sort((item1,item2)-> (item1.length()-item2.length()));
        list=readed;
    }




}
